package handlers

import (
	"database/sql"
	"net/http"
	"strconv"
	"users_management/models"

	"github.com/labstack/echo"
)

type H map[string]interface{}

func GetUsers(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		return c.JSON(http.StatusOK, models.GetUsers(db))
	}
}

func GetUser(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id, _ := strconv.Atoi(c.Param("id"))
		return c.JSON(http.StatusOK, models.GetUser(db, id))
	}
}

func PutUser(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {

		var user models.User

		c.Bind(&user)

		id, err := models.PutUser(db, user.Username, user.Password, user.Name)

		if err == nil {
			return c.JSON(http.StatusCreated, H{
				"created": id,
			})
		} else {
			return err
		}

	}
}

func EditUser(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id, _ := strconv.Atoi(c.Param("id"))
		var user models.User
		c.Bind(&user)

		_, err := models.EditUser(db, id, user.Username, user.Password, user.Name)

		if err == nil {
			return c.JSON(http.StatusOK, H{
				"updated": user,
			})
		} else {
			return err
		}
	}
}

func DeleteUser(db *sql.DB) echo.HandlerFunc {
	return func(c echo.Context) error {
		id, _ := strconv.Atoi(c.Param("id"))

		_, err := models.DeleteUser(db, id)

		if err == nil {
			return c.JSON(http.StatusOK, H{
				"deleted": id,
			})
		} else {
			return err
		}

	}
}
