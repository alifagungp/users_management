package main

import (
	"database/sql"
	"users_management/handlers"

	_ "github.com/go-sql-driver/mysql"
	"github.com/labstack/echo"
)

func main() {
	e := echo.New()
	db := initDB("users")
	migrate(db)

	// daftar api
	e.GET("/user", handlers.GetUsers(db))
	e.GET("/user/:id", handlers.GetUser(db))
	e.POST("/user", handlers.PutUser(db))
	e.PUT("/user/:id", handlers.EditUser(db))
	e.DELETE("/user/:id", handlers.DeleteUser(db))

	e.Logger.Fatal(e.Start(":8000"))
}

func initDB(filepath string) *sql.DB {
	dbDriver := "mysql"
	dbUser := "root"
	dbPass := "232425"
	db, err := sql.Open(dbDriver, dbUser+":"+dbPass+"@/"+filepath)
	if err != nil {
		panic(err.Error())
	}
	return db
}

func migrate(db *sql.DB) {
	sql := `
    CREATE TABLE IF NOT EXISTS users(
        id int NOT NULL AUTO_INCREMENT,
  		username varchar(100) NOT NULL,
  		password varchar(100) NOT NULL,
  		name varchar(100) NOT NULL,
  		PRIMARY KEY (id),
		CONSTRAINT data_unique UNIQUE (id, username)
    );
    `

	_, err := db.Exec(sql)
	// Exit if something goes wrong with our SQL statement above
	if err != nil {
		panic(err)
	}
}
