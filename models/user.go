package models

import (
	"database/sql"
)

type User struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
	Name     string `json:"name"`
}

type UserCollection struct {
	User []User `json:"items"`
}

func GetUsers(db *sql.DB) UserCollection {
	sql := "SELECT * FROM users"
	rows, err := db.Query(sql)
	// Exit jika terjadi error
	if err != nil {
		panic(err)
	}
	// clean rows
	defer rows.Close()

	result := UserCollection{}
	for rows.Next() {
		user := User{}
		err2 := rows.Scan(&user.ID, &user.Username, &user.Password, &user.Name)
		// Exit jika error
		if err2 != nil {
			panic(err2)
		}
		result.User = append(result.User, user)
	}
	return result
}

func GetUser(db *sql.DB, id int) UserCollection {
	sql := "SELECT * FROM users WHERE id = ?;"
	rows, err := db.Query(sql)
	// Exit jika terjadi error
	if err != nil {
		panic(err)
	}
	// clean rows
	defer rows.Close()

	result := UserCollection{}
	for rows.Next() {
		user := User{}
		err2 := rows.Scan(&user.ID, &user.Username, &user.Password, &user.Name)
		// Exit jika error
		if err2 != nil {
			panic(err2)
		}
		result.User = append(result.User, user)
	}
	return result
}

func PutUser(db *sql.DB, username string, password string, name string) (int64, error) {
	sql := "INSERT INTO users(username, password, name) VALUES(?,?,?)"

	// membuat prepared SQL statement
	stmt, err := db.Prepare(sql)
	// Exit jika error
	if err != nil {
		panic(err)
	}
	// memastikan statement ditutup setelah selesai
	defer stmt.Close()

	result, err2 := stmt.Exec(username, password, name)
	// Exit jika error
	if err2 != nil {
		panic(err2)
	}

	return result.LastInsertId()
}

func EditUser(db *sql.DB, id int, username string, password string, name string) (int64, error) {
	sql := "UPDATE users set username = ?, password = ? , name = ? WHERE id = ?"

	stmt, err := db.Prepare(sql)

	if err != nil {
		panic(err)
	}

	result, err2 := stmt.Exec(username, password, name, id)

	if err2 != nil {
		panic(err2)
	}

	return result.RowsAffected()
}

func DeleteUser(db *sql.DB, id int) (int64, error) {
	sql := "DELETE FROM users WHERE id = ?;"

	// buat prepare statement
	stmt, err := db.Prepare(sql)
	// Exit jika error
	if err != nil {
		panic(err)
	}

	result, err2 := stmt.Exec(id)
	// Exit jika error
	if err2 != nil {
		panic(err2)
	}

	return result.RowsAffected()
}
